# Initializing DFX

Run dfx in separate terminal

```
dfx start --clean
```

# Configuring dfx.json

```
{
    "canisters": {
      "vanilla-icp": {
        "frontend": {
          "entrypoint": "out/index.html"
        },
        "source": ["out"],
        "type": "assets"
      }
    },
    "dfx": "0.15.0",
    "version": 1
  }
```

# Deploying your landing page (frontend)

Open VS Code and open a terminal instance and run:

```
dfx deploy
```
